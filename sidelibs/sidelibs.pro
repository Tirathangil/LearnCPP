TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

HEADERS += \
        include\maildbparser.h
SOURCES += \
        main.cpp \
        maildbparser.cpp
